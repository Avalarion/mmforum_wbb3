<?php
/**
 * Created 02.2013 by Bastian Bringenberg <mail@bastian-bringenberg.de>
 * Created for Cherchant.net
 * Origin: git.bbnetz.eu
 * 
 * This Script allows you to export Users, Posts, Threads, Boards and private Messages of an existing mm_forum ( extension of TYPO3 ) and import it into an WBB3.*
 *
 * USAGE
 * To Use this script it will be best to have shell access, simply put the file anywhere of your filesystem, adjust the informations under CONFIGURATION and Run the Script-
 * After importing data it is very important to send yourself a new password.
 *
 *
 * With this command you're able to get your old Data from mysql as you need them both in the new database
 * mysqldump -uoldDatabaseUser -p oldDatabase fe_users tx_mmforum_forums tx_mmforum_pminbox tx_mmforum_posts tx_mmforum_posts_text tx_mmforum_topics tx_mmforum_userfields tx_mmforum_userfields_contents > mm_forum_tables.sql
 *
 * It is also very recommended to let the users update their password as clean md5 is not the bestway 
 *a
 * This Script is under CreativeCommons BY 2.0
 *
 * If you have any problems with this script please send me a message via email or contact me on any other possible channel.
 * If you have additional Changes todo please write me also message, it could be, that 
 * It would be also nice to know how you used this script or if you got any problems about it.
 */

/**
 * CONFIGURATION
 */
$db = mysql_connect('127.0.0.1', 'root', '');
mysql_select_db('myDatabase', $db);
$GLOBALS['prefix_wbb'] = 'wbb1_1_';
$GLOBALS['prefix_wcf'] = 'wcf1_';
$GLOBALS['lineBreak'] = PHP_EOL;
$GLOBALS['myAdminUserID'] = 1; // mm_forum with admin rights ( only one ! )
if(FALSE) $GLOBALS['lineBreak'] = '<br />';

/**
 * If this Value is False every single password will be changed to it's md5-value.
 * @todo possible passwords are salted in TYPO3, there is no connecting possibility
 */
$GLOBALS['used_md5'] = false;


/**
 *      ACTION STARTING
 * == Please don't Change ==
 */
/**
 * @todo Thread if forum exists
 * @todo Post if thread exists
 * @todo USER: SALT???
 * @todo PMCOund, PostCount, ...Count
 * MISSING FIELDS OF FE_USER?
 * @todo attachments?
 */
startMessage();
importUser();
//importUserFields();
//importUserFieldData();
importPrivateMessages();
importBoards();
importThreads();
importPosts();
endMessage();

function startMessage() {
	echo 'Export TYPO3 mm_forum - Import Woltlab Burning Board 3 ( WBB3 )'.$GLOBALS['lineBreak'];
	echo 'Bastian Bringenberg <mail@bastian-bringenberg.de>'.$GLOBALS['lineBreak'];
	echo '==============================================================='.$GLOBALS['lineBreak'];
	echo 'For more informations see git.bbnetz.eu'.$GLOBALS['lineBreak'];
	echo $GLOBALS['lineBreak'];
}

function endMessage() {
	echo $GLOBALS['lineBreak'];
	echo ' = Script Finished = '.$GLOBALS['lineBreak'];
}

/**
 * Imports Users from fe_users to wbb Users
 * 
 * @param void
 * @todo truncate
 * @return void
 */
function importUser() {
	echo 'Start Importing Users!'.$GLOBALS['lineBreak'];
	echo 'Cleaning Tables from old Data'.$GLOBALS['lineBreak'];
	mysql_query('TRUNCATE '.$GLOBALS['prefix_wbb'].'user;');
	mysql_query('TRUNCATE '.$GLOBALS['prefix_wcf'].'user;');
	mysql_query('TRUNCATE '.$GLOBALS['prefix_wcf'].'user_to_groups;');
	$query = "SELECT uid,username,password,email,www,lastlogin,crdate FROM fe_users WHERE disable=0 ORDER BY uid ASC;";
	$result = mysql_query($query);
	$i = 0;
	while($row = mysql_fetch_assoc($result)) {
		echo '+ Got User: '.$row['username'].' ( '.$row['uid'].' )'.$GLOBALS['lineBreak'];
		if(!$GLOBALS['used_md5']) $row['password'] = md5($row['password']);
		mysql_query('INSERT INTO '.$GLOBALS['prefix_wbb'].'user (userID, boardLastVisitTime, boardLastActivityTime) VALUES ('.$row['uid'].', '.$row['lastlogin'].', '.$row['lastlogin'].');');
		mysql_query('INSERT INTO '.$GLOBALS['prefix_wcf'].'user (userID, username, email, password, salt, registrationDate,lastActivityTime) VALUES ('.$row['uid'].', "'.mysql_real_escape_string($row['username']).'", "'.$row['email'].'", "'.$row['password'].'", "", '.$row['crdate'].', '.$row['lastlogin'].');');
		mysql_query('INSERT INTO '.$GLOBALS['prefix_wcf'].'user_to_groups (userID, groupID) VALUES ('.$row['uid'].', 1);');
		$i++;
	}
		/* 
		 * @todo only development
		 */
			mysql_query('INSERT INTO '.$GLOBALS['prefix_wcf'].'user_to_groups (userID, groupID) VALUES ('.$GLOBALS['myAdminUserID'].', 3);');
			mysql_query('INSERT INTO '.$GLOBALS['prefix_wcf'].'user_to_groups (userID, groupID) VALUES ('.$GLOBALS['myAdminUserID'].', 4);');

	echo '+ Got '.$i.' Users. Job Finished.'.$GLOBALS['lineBreak'].$GLOBALS['lineBreak'];
}

/**
 * Imports all existing User Fields from mm_forum and saves them into wcf tables.
 *
 * @param void
 * @return void 
 */
function importUserFields() {
	echo 'Start Importing UserFields!'.$GLOBALS['lineBreak'];
	echo 'Cleaning Tables from old Data'.$GLOBALS['lineBreak'];
	mysql_query('TRUNCATE '.$GLOBALS['prefix_wcf'].'user_option;');
	$query = 'SELECT uid,label FROM tx_mmforum_userfields WHERE hidden = 0 AND deleted = 0 ORDER BY uid ASC;';
	$result = mysql_query($query);
	$i = 0;
	while($row = mysql_fetch_assoc($result)) {
		echo '+ Got Field: '.$row['label'].' ( '.$row['uid'].' )'.$GLOBALS['lineBreak'];
		mysql_query('INSERT INTO '.$GLOBALS['prefix_wcf'].'user_option (optionID, optionname) VALUES ('.(intval($row['uid'])+100).', "'.mysql_real_escape_string($row['label']).'");');
		$i++;
	}
	echo '+ Got '.$i.' Fields. Job Finished.'.$GLOBALS['lineBreak'].$GLOBALS['lineBreak'];

}

/**
 * Imports for each available UserField the Data per User
 *
 * @param void
 * @return void
 * @todo create import UserField
 */
function importUserFieldData() {

}

/**
 * Imports all private Messages for each existing User
 * If readFlag is set, it is read from everyone
 *
 * @param void
 * @return void
 * @todo WCF PrÃ¤fix einbauen
 */
function importPrivateMessages() {
	echo 'Start Importing PrivateMessages'.$GLOBALS['lineBreak'];
	echo 'Cleaning Tables from old Data'.$GLOBALS['lineBreak'];
	mysql_query('TRUNCATE '.$GLOBALS['prefix_wcf'].'pm;');
	mysql_query('TRUNCATE '.$GLOBALS['prefix_wcf'].'pm_to_user;');
	echo '+ Single Output disabled for overview reasons.'.$GLOBALS['lineBreak'];
	$i = 0;
	$query = 'SELECT box.uid, box.from_uid, box.from_name, box.to_uid, box.to_name, box.subject, box.message, box.sendtime, box.read_flg FROM tx_mmforum_pminbox as box JOIN fe_users as users ON users.uid = box.from_uid JOIN fe_users as users2 ON users2.uid = box.to_uid WHERE users.disable = 0 AND users.deleted = 0 AND users2.disable = 0 AND users2.deleted = 0 AND box.hidden=0 AND box.deleted=0 AND box.mess_type = 0;';
	$result = mysql_query($query);
	while($row = mysql_fetch_assoc($result)) {
		$i++;
		$read = 0;
		if($row['read_flg']){
			$readByAll = 1;
			$read = $row['sendtime'];
		}
		mysql_query('INSERT INTO '.$GLOBALS['prefix_wcf'].'pm (pmID, userID, username, subject, message, time, isViewedByAll) 
			VALUES ('.$row['uid'].', '.$row['from_uid'].', "'.mysql_real_escape_string($row['from_name']).'", "'.mysql_real_escape_string($row['subject']).'", "'.mysql_real_escape_string($row['message']).'", '.$row['sendtime'].', '.$readByAll.');');
		mysql_query('INSERT INTO '.$GLOBALS['prefix_wcf'].'pm_to_user (pmID, recipientID, recipient, isViewed) 
			VALUES ('.$row['uid'].', '.$row['to_uid'].', "'.mysql_real_escape_string($row['to_name']).'", '.$read.');');
	}

	echo '+ Got '.$i.' Private Messages. Job Finished.'.$GLOBALS['lineBreak'].$GLOBALS['lineBreak'];
}

/**
 * Imports all Boards
 * If no parent exists it's a category
 *
 * @param void
 * @return void
 */
function importBoards() {
	echo 'Start Importing Boards'.$GLOBALS['lineBreak'];
	echo 'Cleaning Tables from old Data'.$GLOBALS['lineBreak'];
	mysql_query('TRUNCATE '.$GLOBALS['prefix_wbb'].'board;');
	mysql_query('TRUNCATE '.$GLOBALS['prefix_wbb'].'board_structure;');
	$i = 0;
	$query = 'SELECT uid,forum_name,parentID FROM tx_mmforum_forums WHERE deleted = 0 AND hidden = 0 ORDER BY parentID;';
	$result = mysql_query($query);
	while($row = mysql_fetch_assoc($result)) {
		echo '+ Got Board: '.$row['forum_name'].' ( '.$row['uid'].' )'.$GLOBALS['lineBreak'];
		$i++;
		$type = 1;
		if($row['parentID'] != 0) $type = 0;
		mysql_query('INSERT INTO '.$GLOBALS['prefix_wbb'].'board (boardID, parentID, title, boardType) VALUES ('.$row['uid'].', '.$row['parentID'].', "'.mysql_real_escape_string($row['forum_name']).'", '.$type.');');
		mysql_query('INSERT INTO '.$GLOBALS['prefix_wbb'].'board_structure (boardID, parentID, position) VALUES ('.$row['uid'].', '.$row['parentID'].', '.$i.');');
	}
	echo '+ Got '.$i.' Boards. Job Finished.'.$GLOBALS['lineBreak'].$GLOBALS['lineBreak'];
}
/**
 * Imports all Threads
 * @todo first poster?
 * @todo topic_poster?
 * @todo topic_time?
 * @todo firstPostID
 *
 * @param void
 * @return void
 */
function importThreads() {
	echo 'Start Importing Threads'.$GLOBALS['lineBreak'];
	echo 'Cleaning Tables from old Data'.$GLOBALS['lineBreak'];
	mysql_query('TRUNCATE '.$GLOBALS['prefix_wbb'].'thread;');
	echo '+ Single Output disabled for overview reasons.'.$GLOBALS['lineBreak'];
	$i = 0;
	/**
	 * | uid | pid | tstamp     | crdate     | cruser_id | deleted | hidden | topic_title   | topic_poster | topic_time | topic_views | topic_replies | topic_last_post_id | forum_id | topic_first_post_id | topic_is | solved | read_flag | at_top_flag | closed_flag | poll_id | shadow_tid | shadow_fid | tx_mmforumsearch_index_write |
	 */
	$query = 'SELECT t.uid, t.topic_title, t.topic_time, t.forum_id, t.closed_flag FROM tx_mmforum_topics as t JOIN tx_mmforum_forums as b ON t.forum_id = b.uid WHERE t.deleted = 0 AND t.hidden = 0 AND b.deleted = 0 AND b.hidden = 0 ORDER BY t.forum_id ASC;';
	$result = mysql_query($query);
	while($row = mysql_fetch_assoc($result)) {
		/**
		 * | threadID | boardID | languageID | prefix | topic | firstPostID | firstPostPreview| time       | userID | username | lastPostTime | lastPosterID | lastPoster | replies | views | ratings | rating | attachments | polls | isAnnouncement | isSticky | isDisabled | everEnabled | isClosed | isDeleted | movedThreadID | movedTime | deleteTime | deletedBy | deletedByID | deleteReason | isDone | thankCount | 
		 */
		 mysql_query('INSERT INTO '.$GLOBALS['prefix_wbb'].'thread (threadID, boardID, topic, isClosed, time) VALUES ('.$row['uid'].', '.$row['forum_id'].', "'.mysql_real_escape_string($row['topic_title']).'", '.$row['closed_flag'].', '.$row['topic_time'].');');
		$i++;
	}
	echo '+ Got '.$i.' Threads. Job Finished.'.$GLOBALS['lineBreak'].$GLOBALS['lineBreak'];
}
/**
 * Imports all Posts
 * @todo If deleted = 0 and hidden = 0 and thread exists
 *
 * @param void
 * @return void
 * @todo write importPosts
 */
function importPosts() {
	echo 'Start Importing Posts'.$GLOBALS['lineBreak'];
	echo 'Cleaning Tables from old Data'.$GLOBALS['lineBreak'];
	mysql_query('TRUNCATE '.$GLOBALS['prefix_wbb'].'post;');
	echo '+ Single Output disabled for overview reasons.'.$GLOBALS['lineBreak'];
	$i = 0;
	/**
	 * tx_mmforum_posts - tx_mmforum_posts_text
	 * | uid | pid | tstamp     | crdate     | cruser_id | deleted | hidden | topic_id | forum_id | poster_id | post_time  | poster_ip | edit_time  | edit_count | attachment | tx_mmforumsearch_index_write |
	 * | uid | pid | tstamp     | crdate     | cruser_id | deleted | hidden | post_id | post_text | cache_tstamp | cache_text
	 */
	$query = 'SELECT p.uid, p.post_time, p.poster_id, p.topic_id, t.post_text, fe_users.username FROM tx_mmforum_posts as p JOIN tx_mmforum_posts_text as t ON p.uid = t.post_id JOIN tx_mmforum_topics as topics ON topics.uid = p.topic_id JOIN fe_users ON fe_users.uid = p.poster_id WHERE p.deleted = 0 AND t.deleted = 0 AND p.hidden = 0 AND t.hidden = 0 ORDER BY p.topic_id ASC;';
	$result = mysql_query($query);
	while($row = mysql_fetch_assoc($result)) {
		$i++;
		/**
		 * wbb1_1_posts
		 * @todo parentPostID
		 * @todo username
		 */

		mysql_query('INSERT INTO '.$GLOBALS['prefix_wbb'].'post (postID, threadID, userID, username, message, time)
		 VALUES ('.$row['uid'].', '.$row['topic_id'].', '.$row['poster_id'].', "'.mysql_real_escape_string($row['username']).'", "'.mysql_real_escape_string($row['post_text']).'", '.$row['post_time'].');');
	}
	echo '+ Got '.$i.' Posts. Job Finished.'.$GLOBALS['lineBreak'].$GLOBALS['lineBreak'];
}

?>